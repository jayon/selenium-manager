action=$1
selenium_server=selenium-server-standalone-2.37.0.jar

# driver chrome
driver_chrome_version=2.6
driver_chrome_mac=chromedriver
driver_chrome_win=chromedriver.exe

# driver iexplorer
driver_ie_version=IEDriverServer_Win32_2.37.0
driver_ie=IEDriverServer.exe


# ---------------------------------------------------------------------
# install
# ---------------------------------------------------------------------
if [ "$action" == "install" ]
then

  # selenium server
  wget http://selenium.googlecode.com/files/$selenium_server

  # driver chrome
  wget http://chromedriver.storage.googleapis.com/$driver_chrome_version/chromedriver_mac32.zip
  wget http://chromedriver.storage.googleapis.com/$driver_chrome_version/chromedriver_win32.zip

  unzip chromedriver_mac32.zip
  rm chromedriver_mac32.zip

  unzip chromedriver_win32.zip
  rm chromedriver_win32.zip

  # driver iexplorer
  wget https://selenium.googlecode.com/files/$driver_ie_version.zip
  unzip $driver_ie_version.zip
  rm $driver_ie_version.zip

# ---------------------------------------------------------------------
# server
# ---------------------------------------------------------------------
elif [ "$action" == "server" ]
then
  java -jar $selenium_server -role hub

# ---------------------------------------------------------------------
# node
# ---------------------------------------------------------------------
elif [ "$action" == "node" ]
then
  java -jar $selenium_server -role node -hub http://localhost:4444/grid/register

else
  echo ejemplo de uso: $0 install
  echo Metodos: install, server, node
fi
